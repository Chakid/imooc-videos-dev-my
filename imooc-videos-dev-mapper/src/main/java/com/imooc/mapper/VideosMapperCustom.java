package com.imooc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.imooc.pojo.Videos;
import com.imooc.pojo.vo.VideosVO;
import com.imooc.utils.MyMapper;

	
public interface VideosMapperCustom extends MyMapper<Videos> {
	
	public List<VideosVO> queryAllVideos(@Param("videoDesc") String videoDesc,@Param("userId") String userId);
	
	
	/**
	 * 
	 * @param 对视频的喜欢数量进行累加
	 */
	public void addVideoLikeCount(String videoId);
	
	
	/**
	 * 
	 * @param 对视频的喜欢数量进行累减
	 */
	public void reduceVideoLikeCount(String videoId);
	


	/**
	 * @Description: 查询点赞视频
	 */
	public List<VideosVO> queryMyLikeVideos(@Param("userId") String userId);

	/**
	 * @Description: 查询关注的视频
	 */
	public List<VideosVO> queryMyFollowVideos(@Param("userId") String userId);

	
	
	
	
}