package com.imooc.mapper;

import com.imooc.pojo.Users;
import com.imooc.utils.MyMapper;

public interface UsersMapper extends MyMapper<Users> {
	
	
	/**
	 * 
	 * @param 用户受喜欢数量累加
	 */
	public void addReceiveLikeCount(String videoId);
	
	/**
	 * 
	 * @param 用户受喜欢数量累减
	 */
	public void reduceReceiveLikeCount(String videoId);
	
	/**
	 *  增加粉丝数
	 */
	
	public void addFansCount(String userId);
	
	
	/**
	 *  增加关注数
	 */
	
	public void addFollersCount(String userId);
	
	
	/**
	 *  减少粉丝数
	 */
	
	public void reduceFansCount(String userId);
	/**
	 *  减少关注数
	 */
	
	public void reduceFollersCount(String userId);
	
	
}