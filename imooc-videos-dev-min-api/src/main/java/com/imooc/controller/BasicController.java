package com.imooc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.imooc.utils.RedisOperator;



@RestController
public class BasicController {
	
	@Autowired
	public RedisOperator redis;
	
	public static final String USER_REDIS_SESSION="user-redis-session";
	
	//文件保存命名空间
	public static final	String FILE_SPACE="C:/imooc_videos_dev";
	
	//ffmpeg所在目录
	public static final	String FFMPEG_EXE="C:\\imooc_videos_dev\\tools\\ffmpeg\\bin\\ffmpeg";
	
	//每页分页的页数
	public static final Integer PAGE_SIZE=5;
}
