package com.imooc.service;

import com.imooc.pojo.Users;
import com.imooc.pojo.UsersReport;

/**
 * @author Chakid
 *
 */

public interface UserService {
		 
	/**
	 * @param username
	 * @return 
	 * 判断用户名是否存在
	 */
	public boolean queryUsernameIsExist(String username);
	
	/**
	 * @param users
	 * 保存用户信息(用户注册)
	 */
	public void saveUser(Users user);

	
	
	/**
	 * @param username
	 * @param md5Str
	 * @return
	 *   查询用户是否存在 
	 */
	public Users queryUserForLogin(String username, String password);
	
	/**
	 * 
	 *   用户修改信息
	 * @param user
	 */
	public void updateUserInfo(Users user);
	
	/**
	 *   查询用户信息
	 * @param userId
	 * @return
	 */
	public Users queryUserInfo(String userId);
	
	
	/**
	 *  
	 * @param userId
	 * @param videoId
	 * @return  查询用户是否喜欢点赞视频
	 */
	public boolean isUserLikeVideo(String userId,String videoId);
	
	/**
	 *  增加用户和粉丝的关系
	 * @param userId
	 * @param fanId
	 */
	public void saveUserFanRelation(String userId,String fanId);
	
	/**
	 *  删除用户和粉丝的关系
	 * @param userId
	 * @param fanId
	 */
	public void deleteUserFanRelation(String userId,String fanId);
	
	/**
	 *  查询用户是否关注
	 * @param userId
	 * @param fanId
	 */
	public boolean queryIsFollow(String userId,String fanId );
	
	/**
	 * @Description: 举报用户
	 */
	public void reportUser(UsersReport userReport);
	
}

























