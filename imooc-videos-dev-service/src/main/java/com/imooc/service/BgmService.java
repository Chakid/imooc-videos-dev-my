package com.imooc.service;

import java.util.List;

import com.imooc.pojo.Bgm;

/**
 * @author Chakid
 *
 */

public interface BgmService {
		 
	
	/**
	 * 查询背景音乐列表
	 * @return
	 */
	public List<Bgm> queryBgmList();
	
	/**
	 *  根据id来查询bgm的信息
	 * @param bgmId
	 * @return
	 */
	
	public Bgm queryBgmById(String bgmId);
	
}

























